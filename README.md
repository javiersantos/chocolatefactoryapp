## ChocolateFactoryApp
### Napptilus
#### Developed by Javier Santos

![Screenshot](app-screenshot.jpeg)

### Architecture

The project was developed with an architecture based on MVVM (model-view-viewmodel-model), Clean Architecture and dependency injection with Dagger2. It is separated into the following modules:
- app: module with the declaration of the AndroidManifest.xml and the Application class.
- buildSrc: module with the declaration of dependencies and versions for the Gradle files.
- data: module that implements the data layer and is responsible for persisting domain models.
   - model: defines the model objects to be used in the visual layer of the application.
   - remote: contains the service call implementation, as well as the Retrofit implementations, the Dto objects and mappers to the model objects.
   - repository: contains the repositories that connect the viewmodel with the data source (remote or local).
- features: module containing the visual implementation (including activities, fragments, adapters, etc). Each feature is separated under a different package and is responsible for displaying information to the user and interpreting actions.
   - domain: defines the use cases that will be used to connect to the repository.

### Features

- Kotlin :)
- Retrieves the Oompa Loompa list by using Retrofit in combination with Flow and LiveData. It supports pagination too!
    - Clicking on any item will open the details screen.
    - Clicking the expand/collapse icon will show the song of the Oompa Loompa by expanding the card.
- Filter data by profession and gender.
- Remotes images are cached locally.
- Handles error when no internet connection is available and it shows a custom screen with a retry button.
- Some tests are included in the data layer.
- Uses modern libraries and features, such as Navigation Component and Flow.

### Dependencies

- AppCompat: includes a set of libraries that allow the application to run on new and old versions of Android.
- ConstraintLayout: allows you to create layouts and interfaces. It is used as the basis for the creation of XML interfaces in the project.
- MaterialDialog: includes visual components of the Material Design layer. In this project this library will add the CardView feature to the list of employees and shows an AppBarLayout / CollapsingToolbarLayout to the detail screen.
- PullToRefresh: allows to include the "pull-to-refresh" functionality to the application, which allows to control the refreshing of views in a standardized way. It adds a pull to refresh feature to the list of employees.
- LifeCycleLiveData: required for the use of LiveData in the app. LiveData is a component that handles data optimized for the Android lifecycle and it will be used on the ViewModels to receive and subscribe data on remote/local changes.
- LifeCycleViewModel required for the use of ViewModel in the architecture. ViewModel is optimized for Android lifecycle (for example, on device orientation changes).
- Navigation: library that adds support to Navigation Component to simplify navigation in the app through XML objects. Safeargs is also used to use Directions and pass arguments to navigation. The navigation of the app has been done with Navigation Component and the navigation is defined in `nav_home`.
- Glide: adds compatibility for remote images on ImageView and many other features to display images in the user interface. It is used on the ViewHolder and detail screen to display the profile icon of the Oompa Loompa.
- Retrofit: adds support for service calls and handles responses.
- Gson / Gson Converter: allows to convert a JSON format to a model and vice versa. It is necessary on Retrofit to parse de JSON.
- Dagger: adds dependency injection to the project.
- Kotlin KTX / Kotlin Coroutines: adds a set of extensions to simplify and enhance Kotlin app development. Kotlin Coroutines simplifies the creation of asynchronous functions. The project uses the `suspend` method to handle data asynchronously and Flow to abstract LiveData from the data layer.
- JUnit: framework for the creation of unit tests.
- Mockito: allows you to mock objects for unit testing. In this project it will be used to mock the EmployeeWs interface.
- Kluent: simplifies the creation of tests by allowing the use of nomenclature such as `shouldBeEqualTo`, `shouldBeEmpty` or `shouldBeNullOrEmpty`, for example.
- CoroutinesTest: allows coroutines to be tested in tests. `runBlockingTest` in the test declaration.

### Testing

The `remote` module of the `data` layer includes some unit tests, for which JUnit, Mockito, Kluent and CoroutinesTest dependencies have been used.

- EmployeeDtoTest: test that checks the functionality of the JSON parse with the corresponding Dto object.
- EmployeeRemoteDataSourceImplTest: test that checks the DataSource implementation.