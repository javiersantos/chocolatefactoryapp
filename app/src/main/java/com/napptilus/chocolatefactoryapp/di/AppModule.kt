package com.napptilus.chocolatefactoryapp.di

import android.content.Context
import com.napptilus.chocolatefactoryapp.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApplication(application: App): Context = application

}