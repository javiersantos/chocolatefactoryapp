package com.napptilus.chocolatefactoryapp.di

import com.napptilus.chocolatefactoryapp.App
import com.napptilus.data.remote.employee.di.RemoteModule
import com.napptilus.data.repository.di.RepositoryModule
import com.napptilus.features.common.di.MainActivityBuilder
import com.napptilus.features.detail.di.DetailFragmentBuilder
import com.napptilus.features.detail.di.DetailModule
import com.napptilus.features.home.di.HomeFragmentBuilder
import com.napptilus.features.home.di.HomeModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        RemoteModule::class,
        HomeModule::class,
        DetailModule::class,
        MainActivityBuilder::class,
        RepositoryModule::class,
        HomeFragmentBuilder::class,
        DetailFragmentBuilder::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

    fun inject(application: App)
}