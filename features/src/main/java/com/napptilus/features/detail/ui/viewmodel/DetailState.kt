package com.napptilus.features.detail.ui.viewmodel

import com.napptilus.data.model.employee.bo.EmployeeBo
import java.lang.Exception

sealed class DetailState {
    object Loading : DetailState()
    class ShowEmployee(val item: EmployeeBo) : DetailState()
    class ShowError(val exception: Exception) : DetailState()
}