package com.napptilus.features.detail.ui.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.napptilus.data.model.employee.bo.EmployeeBo
import com.napptilus.features.R
import com.napptilus.features.common.di.ViewModelFactory
import com.napptilus.features.common.ui.MainActivity
import com.napptilus.features.common.util.*
import com.napptilus.features.databinding.FragmentDetailBinding
import com.napptilus.features.detail.ui.viewmodel.DetailState
import com.napptilus.features.detail.ui.viewmodel.DetailViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class DetailFragment : Fragment() {

    companion object {
        const val TAG = "DetailFragment"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<DetailViewModel>
    private val viewModel: DetailViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[DetailViewModel::class.java]
    }

    private var binding: FragmentDetailBinding? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        configureObservers()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).hideStatusBar()
        makeRequest()
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    private fun makeRequest() {
        arguments?.let {
            viewModel.requestEmployeeDetail(DetailFragmentArgs.fromBundle(it).employeeId)
        }
    }

    private fun configureObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                DetailState.Loading -> {
                    Log.e(TAG, "DetailState.Loading")
                    showLoading(true)
                }
                is DetailState.ShowError -> {
                    Log.e(TAG, state.exception.message.orEmpty())
                    showLoading(false)
                    context?.showErrorView(view, ::makeRequest)
                }
                is DetailState.ShowEmployee -> {
                    Log.e(TAG, "DetailState.ShowList")
                    showLoading(false)
                    renderEmployee(state.item)
                }
            }
        }
    }
    
    private fun renderEmployee(employee: EmployeeBo) {
        binding?.let {
            Glide.with(requireContext())
                .load(employee.image)
                .into(it.detailImgHeader)
            binding?.detailLabelName?.text = employee.firstName
            binding?.detailContainerMain?.detailLabelMail?.text = employee.email
            binding?.detailContainerMain?.detailLabelAge?.text = getString(R.string.profile_age, employee.age)
        }
    }

    private fun showLoading(loading: Boolean) {
        binding?.detailContainerMain?.detailContainerLoading?.homeImgLoading?.visibility(loading)
        binding?.detailContainerMain?.detailContainerLoading?.homeImgLoading?.showAnimation(loading)
    }
}