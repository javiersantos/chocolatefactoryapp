package com.napptilus.features.detail.di

import com.napptilus.data.repository.employee.EmployeeRepository
import com.napptilus.features.detail.domain.GetEmployeeDetailUseCase
import com.napptilus.features.detail.domain.GetEmployeeDetailUseCaseImpl
import dagger.Module
import dagger.Provides

@Module
class DetailModule {

    @Provides
    fun getEmployeeDetailUseCaseProvider(employeeRepository: EmployeeRepository) =
        GetEmployeeDetailUseCaseImpl(employeeRepository) as GetEmployeeDetailUseCase

}