package com.napptilus.features.detail.domain

import com.napptilus.data.model.employee.bo.EmployeeBo
import com.napptilus.data.repository.employee.EmployeeRepository
import kotlinx.coroutines.flow.Flow

interface GetEmployeeDetailUseCase {
    suspend fun invoke(id: Int): Flow<EmployeeBo>
}

internal class GetEmployeeDetailUseCaseImpl(
    private val repository: EmployeeRepository
) : GetEmployeeDetailUseCase {

    override suspend fun invoke(id: Int): Flow<EmployeeBo> {
        return repository.getDetails(id)
    }

}