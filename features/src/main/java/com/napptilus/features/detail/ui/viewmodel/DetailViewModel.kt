package com.napptilus.features.detail.ui.viewmodel

import androidx.lifecycle.*
import com.napptilus.data.remote.employee.exception.NetworkException
import com.napptilus.features.detail.domain.GetEmployeeDetailUseCase
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val getEmployeeDetailUseCase: GetEmployeeDetailUseCase
) : ViewModel() {

    private val _state = MutableLiveData<DetailState>()
    val state: LiveData<DetailState>
        get() = _state

    fun requestEmployeeDetail(id: Int) {
        launchRemoteLoad {
            getEmployeeDetailUseCase.invoke(id).collectLatest {
                _state.value = DetailState.ShowEmployee(it)
            }
        }
    }

    private fun launchRemoteLoad(block: suspend() -> Unit): Job {
        return viewModelScope.launch {
            try {
                _state.value = DetailState.Loading
                block()
            } catch (error: Exception) {
                _state.value = DetailState.ShowError(error)
            }
        }
    }

}