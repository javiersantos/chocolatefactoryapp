package com.napptilus.features.detail.di

import com.napptilus.features.detail.ui.fragment.DetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class DetailFragmentBuilder {

    @ContributesAndroidInjector
    internal abstract fun bindDetailFragment(): DetailFragment

}