package com.napptilus.features.home.ui.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.napptilus.features.R
import com.napptilus.features.common.di.ViewModelFactory
import com.napptilus.features.common.ui.MainActivity
import com.napptilus.features.common.util.*
import com.napptilus.features.databinding.FragmentHomeBinding
import com.napptilus.features.home.ui.adapter.EmployeesAdapter
import com.napptilus.features.home.ui.adapter.Order
import com.napptilus.features.home.ui.viewmodel.HomeState
import com.napptilus.features.home.ui.viewmodel.HomeViewModel
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class HomeFragment : Fragment() {

    companion object {
        const val TAG = "HomeFragment"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory<HomeViewModel>
    private val viewModel: HomeViewModel by lazy {
        ViewModelProvider(this, viewModelFactory)[HomeViewModel::class.java]
    }

    private var binding: FragmentHomeBinding? = null
    private var adapter: EmployeesAdapter? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding?.homeRefreshLoading?.setOnRefreshListener { makeRequest() }
        setHasOptionsMenu(true)
        configureObservers()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).setSupportActionBar(binding?.homeToolbarMain)
        (activity as MainActivity).showStatusBar()
        adapter = EmployeesAdapter {
            Navigation.findNavController(view).navigate(
                HomeFragmentDirections.actionHomeFragmentToDetailFragment(it.id)
            )
        }
        binding?.homeListEmployee?.layoutManager = LinearLayoutManager(context)
        binding?.homeListEmployee?.adapter = adapter
        binding?.homeListEmployee?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    viewModel.requestMoreEmployeeList()
                }
            }
        })
        makeRequest()
    }

    override fun onDestroyView() {
        binding = null
        super.onDestroyView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_filter_gender -> {
                viewModel.sortList(Order.GENDER)
            }
            R.id.action_filter_profession -> {
                viewModel.sortList(Order.PROFESSION)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun makeRequest() {
        binding?.homeRefreshLoading?.isRefreshing = false
        viewModel.requestEmployeeList()
    }

    private fun configureObservers() {
        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                HomeState.Loading -> {
                    Log.e(TAG, "HomeState.Loading")
                    showLoading(true)
                }
                is HomeState.ShowError -> {
                    Log.e(TAG, state.exception.message.orEmpty())
                    showLoading(false)
                    context?.showErrorView(view, ::makeRequest)
                }
            }
        }
        viewModel.list.observe(viewLifecycleOwner) {
            Log.e(TAG, "List received")
            showLoading(false)
            adapter?.submitList(it)
        }
    }

    private fun showLoading(loading: Boolean) {
        binding?.homeContainerLoading?.homeImgLoading?.visibility(loading)
        binding?.homeContainerLoading?.homeImgLoading?.showAnimation(loading)
    }
}