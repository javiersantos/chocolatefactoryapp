package com.napptilus.features.home.di

import com.napptilus.features.home.ui.fragment.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeFragmentBuilder {

    @ContributesAndroidInjector
    internal abstract fun bindHomeFragment(): HomeFragment

}