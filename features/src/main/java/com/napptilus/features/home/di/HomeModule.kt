package com.napptilus.features.home.di

import com.napptilus.data.repository.employee.EmployeeRepository
import com.napptilus.features.home.domain.GetEmployeeListUseCase
import com.napptilus.features.home.domain.GetEmployeeListUseCaseImpl
import dagger.Module
import dagger.Provides

@Module
class HomeModule {

    @Provides
    fun getEmployeeListUseCaseProvider(employeeRepository: EmployeeRepository) =
        GetEmployeeListUseCaseImpl(employeeRepository) as GetEmployeeListUseCase

}