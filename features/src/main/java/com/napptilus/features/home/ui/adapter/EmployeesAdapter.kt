package com.napptilus.features.home.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.napptilus.data.model.employee.bo.EmployeeBo
import com.napptilus.features.common.util.hide
import com.napptilus.features.common.util.show
import com.napptilus.features.databinding.RowEmployeeBinding

class EmployeesAdapter(private val callback: (EmployeeBo) -> Unit): ListAdapter<EmployeeBo, EmployeesAdapter.EmployeeViewHolder>(EmployeeDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder =
        EmployeeViewHolder(RowEmployeeBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) =
        holder.bind(currentList[position])

    inner class EmployeeViewHolder(private val binding: RowEmployeeBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                if (adapterPosition != RecyclerView.NO_POSITION) {
                    callback(currentList[adapterPosition])
                }
            }
        }

        fun bind(employeeBo: EmployeeBo) {
            Glide.with(binding.employeeImgPic.context)
                .load(employeeBo.image)
                .into(binding.employeeImgPic)
            binding.employeeLabelFirstName.text = employeeBo.firstName
            binding.employeeLabelLastName.text = employeeBo.lastName
            binding.employeeLabelProfession.text = employeeBo.profession
            binding.employeeLabelSong.text = employeeBo.favorite.song

            binding.employeeContainerExpand.setOnClickListener {
                binding.employeeContainerCollapse.show()
                binding.employeeContainerExpand.hide()
                binding.employeeLabelSong.show()
            }

            binding.employeeContainerCollapse.setOnClickListener {
                binding.employeeContainerCollapse.hide()
                binding.employeeContainerExpand.show()
                binding.employeeLabelSong.hide()
            }
        }
    }
}

class EmployeeDiffCallback : DiffUtil.ItemCallback<EmployeeBo>() {
    override fun areItemsTheSame(oldItem: EmployeeBo, newItem: EmployeeBo): Boolean =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: EmployeeBo, newItem: EmployeeBo): Boolean =
        oldItem == newItem
}

enum class Order {
    GENDER,
    PROFESSION,
    NONE
}