package com.napptilus.features.home.domain

import com.napptilus.data.model.employee.bo.EmployeeListBo
import com.napptilus.data.repository.employee.EmployeeRepository
import kotlinx.coroutines.flow.Flow

interface GetEmployeeListUseCase {
    suspend fun invoke(page: Int): Flow<EmployeeListBo>
}

internal class GetEmployeeListUseCaseImpl(
    private val repository: EmployeeRepository
) : GetEmployeeListUseCase {

    override suspend fun invoke(page: Int): Flow<EmployeeListBo> {
        return repository.getEmployees(page)
    }

}