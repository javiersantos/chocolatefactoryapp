package com.napptilus.features.home.ui.viewmodel

import java.lang.Exception

sealed class HomeState {
    object Loading : HomeState()
    class ShowError(val exception: Exception) : HomeState()
}