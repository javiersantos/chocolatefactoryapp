package com.napptilus.features.home.ui.viewmodel

import androidx.lifecycle.*
import com.napptilus.data.model.employee.bo.EmployeeBo
import com.napptilus.features.home.domain.GetEmployeeListUseCase
import com.napptilus.features.home.ui.adapter.Order
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val getEmployeeListUseCase: GetEmployeeListUseCase
) : ViewModel() {

    companion object {
        const val PAGE_START_INDEX = 1
    }

    private val _state = MutableLiveData<HomeState>()
    val state: LiveData<HomeState>
        get() = _state

    private val _list = MutableLiveData<List<EmployeeBo>>()
    val list: LiveData<List<EmployeeBo>>
        get() = _list

    private var _currentPage = PAGE_START_INDEX
    private var _lastPage = PAGE_START_INDEX

    fun requestEmployeeList() {
        launchRemoteLoad {
            getEmployeeListUseCase.invoke(PAGE_START_INDEX).collectLatest {
                _list.value = it.list
                _currentPage = it.currentPage
                _lastPage = it.lastPage
            }
        }
    }

    fun requestMoreEmployeeList() {
        launchRemoteLoad {
            if (_currentPage < _lastPage) {
                getEmployeeListUseCase.invoke(_currentPage + 1).collectLatest {
                    val currentList = _list.value.orEmpty().toMutableList()
                    currentList.addAll(it.list)
                    _list.value = currentList
                    _currentPage = it.currentPage
                    _lastPage = it.lastPage
                }
            }
        }
    }

    fun sortList(order: Order) {
        when (order) {
            Order.GENDER -> _list.value = _list.value?.sortedByDescending { it.gender }
            Order.PROFESSION -> _list.value = _list.value?.sortedByDescending { it.profession }
        }
    }

    private fun launchRemoteLoad(block: suspend() -> Unit): Job {
        return viewModelScope.launch {
            try {
                _state.value = HomeState.Loading
                block()
            } catch (error: Exception) {
                _state.value = HomeState.ShowError(error)
            }
        }
    }

}