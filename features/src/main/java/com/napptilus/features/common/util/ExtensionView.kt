package com.napptilus.features.common.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView

import android.view.animation.LinearInterpolator

import android.view.animation.Animation

import android.view.animation.RotateAnimation
import com.napptilus.features.R

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.visibility(show: Boolean) {
    this.visibility = if (show) View.VISIBLE else View.GONE
}

fun Context.showErrorView(parent: View?, callback: () -> Unit) {
    parent?.let { container ->
        val errorView =
            LayoutInflater.from(this).inflate(R.layout.view_error, container as ViewGroup, false)
        container.addView(errorView)

        val retryBtn = errorView.findViewById<Button>(R.id.error__btn__retry)
        retryBtn.setOnClickListener {
            container.removeView(errorView)
            callback()
        }
    }
}

fun ImageView.showAnimation(loading: Boolean) {
    if (loading) {
        val rotate =
            RotateAnimation(0f, 180f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotate.duration = 5000
        rotate.interpolator = LinearInterpolator()
        this.startAnimation(rotate)
    } else {
        this.clearAnimation()
    }
}