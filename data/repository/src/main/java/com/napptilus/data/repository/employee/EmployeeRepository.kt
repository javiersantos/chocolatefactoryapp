package com.napptilus.data.repository.employee

import com.napptilus.data.datasource.employee.EmployeeRemoteDataSource
import com.napptilus.data.model.employee.bo.EmployeeBo
import com.napptilus.data.model.employee.bo.EmployeeListBo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface EmployeeRepository {
    suspend fun getEmployees(page: Int): Flow<EmployeeListBo>
    suspend fun getDetails(id: Int): Flow<EmployeeBo>
}

internal class EmployeeRepositoryImpl(
    private val remote: EmployeeRemoteDataSource
) : EmployeeRepository {

    override suspend fun getEmployees(page: Int): Flow<EmployeeListBo> = flow {
        emit(remote.getEmployees(page))
    }

    override suspend fun getDetails(id: Int): Flow<EmployeeBo> = flow {
        emit(remote.getDetails(id))
    }

}