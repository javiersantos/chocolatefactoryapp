package com.napptilus.data.repository.di

import com.napptilus.data.datasource.employee.EmployeeRemoteDataSource
import com.napptilus.data.repository.employee.EmployeeRepository
import com.napptilus.data.repository.employee.EmployeeRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun employeeRepositoryProvider(remote: EmployeeRemoteDataSource) =
        EmployeeRepositoryImpl(remote) as EmployeeRepository

}