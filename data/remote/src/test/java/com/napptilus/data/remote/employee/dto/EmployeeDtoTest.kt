package com.napptilus.data.remote.employee.dto

import com.google.gson.Gson
import com.napptilus.data.remote.employee.readResourceFile
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test

class EmployeeDtoTest {

    @Test
    fun `should parse employee into dto`() {
        val jsonResponse = readResourceFile("employee.json")
        val employeeDto = Gson().fromJson(jsonResponse, EmployeeDto::class.java)

        employeeDto shouldBeEqualTo EmployeeDto(
            lastName = "Karadzas",
            image = "https://s3.eu-central-1.amazonaws.com/napptilus/level-test/1.jpg",
            age = 21,
            id = 0,
            country = "Loompalandia",
            email = "mkaradzas1@visualengin.com",
            gender = "F",
            favorite = FavoriteDto(
                color = "red",
                food = "Chocolat",
                song = "My song"
            ),
            firstName = "Marcy",
            profession = "Developer",
            height = 99
        )
    }

}