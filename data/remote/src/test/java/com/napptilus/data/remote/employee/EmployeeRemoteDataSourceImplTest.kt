package com.napptilus.data.remote.employee

import com.napptilus.data.datasource.employee.EmployeeRemoteDataSource
import com.napptilus.data.remote.employee.dto.EmployeeListDto
import com.nhaarman.mockitokotlin2.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldBeEqualTo
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class EmployeeRemoteDataSourceImplTest {

    private val employeeWs: EmployeeWs = mock()

    private lateinit var employeeRemoteDataSource: EmployeeRemoteDataSource

    @Before
    fun setUp() {
        employeeRemoteDataSource = EmployeeRemoteDataSourceImpl(employeeWs)
    }

    @After
    fun tearDown() {
        verifyNoMoreInteractions(employeeWs)
    }

    @Test
    fun `should return employees when request has been successful`() = runBlockingTest {
        whenever(employeeWs.getEmployees(1)) doReturn EmployeeListDto(1, 20, emptyList())

        val actualEmployees = employeeRemoteDataSource.getEmployees(1)

        verify(employeeWs).getEmployees(1)
        actualEmployees shouldBeEqualTo emptyList()
    }

}