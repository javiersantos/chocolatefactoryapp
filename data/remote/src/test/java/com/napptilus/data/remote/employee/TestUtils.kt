package com.napptilus.data.remote.employee

fun readResourceFile(path: String): String {
    return object {}.javaClass.classLoader?.getResource(path)?.readText() ?: ""
}