package com.napptilus.data.remote.employee

import com.napptilus.data.model.employee.bo.EmployeeBo
import com.napptilus.data.model.employee.bo.EmployeeListBo
import com.napptilus.data.model.employee.bo.FavoriteBo
import com.napptilus.data.remote.employee.dto.EmployeeDto
import com.napptilus.data.remote.employee.dto.EmployeeListDto
import com.napptilus.data.remote.employee.dto.FavoriteDto

internal fun EmployeeListDto.toBo() = EmployeeListBo(
    currentPage = current,
    lastPage = total,
    list = results.map { it.toBo() }
)

internal fun EmployeeDto.toBo() = EmployeeBo(
    id,
    firstName,
    lastName,
    favorite.toBo(),
    gender,
    image,
    profession,
    email,
    age,
    country,
    height
)

private fun FavoriteDto.toBo() = FavoriteBo(
    color,
    food,
    song
)