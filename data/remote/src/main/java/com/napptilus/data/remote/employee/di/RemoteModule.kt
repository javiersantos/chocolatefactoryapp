package com.napptilus.data.remote.employee.di

import com.napptilus.data.datasource.employee.EmployeeRemoteDataSource
import com.napptilus.data.remote.employee.EmployeeRemoteDataSourceImpl
import com.napptilus.data.remote.employee.EmployeeWs
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RemoteModule {

    companion object {
        private const val BASE_URL = "https://2q2woep105.execute-api.eu-west-1.amazonaws.com"
    }

    @Provides
    @Singleton
    fun retrofitProvider(): Retrofit {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.HEADERS
        }

        val client = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

        return Retrofit.Builder()
            .client(client)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    @Provides
    fun employeeWsProvider(retrofit: Retrofit) =
        retrofit.create(EmployeeWs::class.java)

    @Provides
    fun employeeRemoteDataSourceProvider(employeeWs: EmployeeWs) =
        EmployeeRemoteDataSourceImpl(employeeWs) as EmployeeRemoteDataSource

}