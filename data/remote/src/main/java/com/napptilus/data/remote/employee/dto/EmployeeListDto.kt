package com.napptilus.data.remote.employee.dto

import com.google.gson.annotations.SerializedName

data class EmployeeListDto(
    @SerializedName("current")
    val current: Int,
    @SerializedName("total")
    val total: Int,
    @SerializedName("results")
    val results: List<EmployeeDto>
)