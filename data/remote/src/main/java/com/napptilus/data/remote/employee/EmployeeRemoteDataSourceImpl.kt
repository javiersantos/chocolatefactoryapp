package com.napptilus.data.remote.employee

import com.napptilus.data.datasource.employee.EmployeeRemoteDataSource
import com.napptilus.data.model.employee.bo.EmployeeBo

class EmployeeRemoteDataSourceImpl(
    private val employeeWs: EmployeeWs
) : EmployeeRemoteDataSource {

    override suspend fun getEmployees(page: Int) =
        employeeWs.getEmployees(page).toBo()

    override suspend fun getDetails(id: Int): EmployeeBo =
        employeeWs.getDetails(id).toBo()

}