package com.napptilus.data.remote.employee

import com.napptilus.data.remote.employee.dto.EmployeeDto
import com.napptilus.data.remote.employee.dto.EmployeeListDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface EmployeeWs {

    @GET("napptilus/oompa-loompas")
    suspend fun getEmployees(@Query("page") page: Int): EmployeeListDto

    @GET("napptilus/oompa-loompas/{id}")
    suspend fun getDetails(@Path("id") id: Int): EmployeeDto

}