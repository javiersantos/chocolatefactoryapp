package com.napptilus.data.model.employee.bo

data class EmployeeBo(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val favorite: FavoriteBo,
    val gender: String,
    val image: String,
    val profession: String,
    val email: String,
    val age: Int,
    val country: String,
    val height: Int
)