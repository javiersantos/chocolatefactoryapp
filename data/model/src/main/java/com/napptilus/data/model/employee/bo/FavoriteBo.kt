package com.napptilus.data.model.employee.bo

data class FavoriteBo(
    val color: String,
    val food: String,
    val song: String
)