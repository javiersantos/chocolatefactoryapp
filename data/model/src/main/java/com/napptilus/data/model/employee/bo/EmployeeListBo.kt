package com.napptilus.data.model.employee.bo

data class EmployeeListBo(
    val currentPage: Int,
    val lastPage: Int,
    val list: List<EmployeeBo>
)