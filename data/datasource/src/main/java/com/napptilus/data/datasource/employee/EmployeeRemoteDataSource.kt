package com.napptilus.data.datasource.employee

import com.napptilus.data.model.employee.bo.EmployeeBo
import com.napptilus.data.model.employee.bo.EmployeeListBo

interface EmployeeRemoteDataSource {

    suspend fun getEmployees(page: Int): EmployeeListBo

    suspend fun getDetails(id: Int): EmployeeBo

}