package com.napptilus.dependencies

class Dependencies {

    object AndroidX {
        const val AppCompat = "androidx.appcompat:appcompat:${Versions.AndroidX.AppCompat}"
        const val ConstraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.AndroidX.ConstraintLayout}"
        const val MaterialDialog = "com.google.android.material:material:${Versions.AndroidX.MaterialDesign}"
        const val PullToRefresh = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.AndroidX.PullToRefresh}"
        const val LifeCycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.AndroidX.LifeCycle}"
        const val LifeCycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.AndroidX.LifeCycle}"
        const val Navigation = "androidx.navigation:navigation-ui-ktx:${Versions.AndroidX.Navigation}"
        const val NavigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.AndroidX.Navigation}"
    }

    object Android {
        const val Glide = "com.github.bumptech.glide:glide:${Versions.Android.Glide}"
    }

    object Retrofit {
        const val Core = "com.squareup.retrofit2:retrofit:${Versions.Retrofit.Retrofit}"
        const val GsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.Retrofit.Retrofit}"
        const val LoggingInterceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.Retrofit.LoggingInterceptor}"
    }

    object Dagger {
        const val Core = "com.google.dagger:dagger:${Versions.Dagger.Dagger}"
        const val CoreCompiler = "com.google.dagger:dagger-compiler:${Versions.Dagger.Dagger}"
        const val AndroidCompiler = "com.google.dagger:dagger-android-processor:${Versions.Dagger.Dagger}"
        const val AndroidSupport = "com.google.dagger:dagger-android-support:${Versions.Dagger.Dagger}"
    }

    object Kotlin {
        const val KotlinStd = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.Kotlin.Kotlin}"
        const val CoreKtx = "androidx.core:core-ktx:${Versions.Kotlin.CoreKtx}"
        const val CoroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.Kotlin.Coroutines}"
        const val CoroutinesCore = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Versions.Kotlin.Coroutines}"
    }

    object Test {
        const val JUnit = "junit:junit:${Versions.Test.JUnit}"
        const val Mockito = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.Test.Mockito}"
        const val Kluent = "org.amshove.kluent:kluent-android:${Versions.Test.Kluent}"
        const val CoroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.Kotlin.Coroutines}"
    }

}