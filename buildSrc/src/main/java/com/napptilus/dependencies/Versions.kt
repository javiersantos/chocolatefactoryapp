package com.napptilus.dependencies

object Versions {

    object Build {
        const val Gradle = "7.0.2"
        const val CompileSdk = 30
        const val MinSdk = 21
        const val TargetSdk = 30
    }

    object AndroidX {
        const val AppCompat = "1.3.1"
        const val ConstraintLayout = "2.1.0"
        const val MaterialDesign = "1.3.0"
        const val PullToRefresh = "1.0.0"
        const val LifeCycle = "2.3.1"
        const val Navigation = "2.3.5"
    }

    object Retrofit {
        const val Retrofit = "2.9.0"
        const val LoggingInterceptor = "4.9.0"
    }

    object Dagger {
        const val Dagger = "2.38.1"
    }

    object Android {
        const val Glide = "4.12.0"
    }

    object Kotlin {
        const val Kotlin = "1.5.31"
        const val CoreKtx = "1.6.0"
        const val Coroutines = "1.4.3"
    }

    object Test {
        const val JUnit = "4.13"
        const val Mockito = "2.2.0"
        const val Kluent = "1.68"
    }

}